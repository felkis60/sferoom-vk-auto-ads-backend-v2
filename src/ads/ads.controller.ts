import { Controller, Get, Post, Body, Param, Query, ParseArrayPipe } from '@nestjs/common';
// import { AdsService } from './ads.service';
import { VK_API } from 'src/constants';
import { Inject, Injectable } from '@nestjs/common';

@Controller('ads')
export class AdsController {
  constructor(@Inject(VK_API) private vk: any) { }

  @Get('countries')
  getCountries(@Query() query: any) {
    return this.vk.user_api.request('database.getCountries', query)
  }

  @Get('accounts')
  getAccounts(@Query() query: any) {
    return this.vk.user_api.request('ads.getAccounts', query)
  }

  @Get('clients')
  getClients(@Query() query: any) {
    return this.vk.user_api.request('ads.getClients', query)
  }

  @Get('campaigns')
  async getCampaigns(
    @Query('account_id') account_id,
    @Query('access_token') access_token,
    @Query('campaign_ids', new ParseArrayPipe({ optional: true, items: String, separator: ',' })) campaign_ids,
    @Query('client_id') client_id?) {

    //   if (!account_id) {
    //     response.setHeader('Content-Type', 'application/json');
    //     return response.end(JSON.stringify({}));
    //   }

    let req: any = {
      account_id: account_id,
      campaign_ids: campaign_ids ? JSON.stringify(campaign_ids) : 'null',
      access_token: access_token,
    }
    if (client_id) {
      req.client_id = client_id
    }
    let res = await this.vk.user_api.request('ads.getCampaigns', req)
    let campaigns: any[] = res.response

    if (client_id) {
      campaigns.map(c => { c.client_id = client_id; return c })
    }

    req = {
      account_id: account_id,
      ids_type: 'campaign',
      ids: campaigns.map(c => c.id),
      period: 'overall',
      date_from: 0,
      date_to: 0,
      stats_fields: 'ctr',
      access_token: access_token
    }
    res = await this.vk.user_api.request('ads.getStatistics', req)
    for (let i = 0; i < campaigns.length; i++) {
      let stats = res.response[i].stats.length ? res.response[i].stats[0] : {}
      campaigns[i].spent = stats.spent ? stats.spent : 0
      campaigns[i].impressions = stats.impressions ? stats.impressions : 0
      campaigns[i].clicks = stats.clicks ? stats.clicks : 0
      campaigns[i].ctr = stats.ctr
    }
    console.log(campaigns)
    return campaigns
  }

  @Get('ads')
  async getAds(
    @Query('account_id') account_id,
    @Query('access_token') access_token,
    @Query('campaign_id') campaign_id,
    @Query('ad_ids', new ParseArrayPipe({ optional: true, items: String, separator: ',' })) ad_ids,
    @Query('client_id') client_id?
  ) {
  
    let req: any = {
      account_id: account_id,
      campaign_ids: JSON.stringify([campaign_id]),
      ad_ids: ad_ids ? JSON.stringify(ad_ids) : 'null',
      access_token: access_token
    }
    if (client_id) {
      req.client_id = client_id
    }
    let res = await this.vk.user_api.request('ads.getAds', req)
  
    let ads = res.response.map(ad => {
      ad.cpm = ad.cpm / 100
      ad.autobidding_max_cost = ad.autobidding_max_cost / 100
      return ad
    })
  
    req = {
      account_id: account_id,
      ids_type: 'ad',
      ids: ads.map(ad => ad.id),
      period: 'overall',
      date_from: 0,
      date_to: 0,
      stats_fields: 'ctr',
      access_token: access_token
    }
    res = await this.vk.user_api.request('ads.getStatistics', req)
    for (let i = 0; i < ads.length; i++) {
      let stats = res.response[i].stats[0]
      ads[i].spent = stats.spent ? stats.spent : 0
      ads[i].impressions = stats.impressions ? stats.impressions : 0
      ads[i].clicks = stats.clicks ? stats.clicks : 0
      ads[i].ctr = stats.ctr
      ads[i].effective_cost_per_click = stats.effective_cost_per_click
    }
  
    return ads
  }

}
