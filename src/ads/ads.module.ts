import { Module } from '@nestjs/common';
import { AdsController } from './ads.controller';
import { VkModule } from 'src/vk/vk.module';

@Module({
    imports: [VkModule],
    controllers: [AdsController],
})
export class AdsModule {}