import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { AdsModule } from './ads/ads.module';
import { AudioModule } from './audio/audio.module';

@Module({
  imports: [UserModule,ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: ['.env.local'],
  }), AdsModule, AudioModule]
})
export class AppModule {}
