import { Controller, Get, Post, Body, Param, Query, ParseArrayPipe } from '@nestjs/common';
import { AudioService } from './audio.service';


@Controller('audio')
export class AudioController {
  constructor(private readonly audioService: AudioService) {}

  @Get('adds')
  get(@Query('audio_full_ids', new ParseArrayPipe({ items: String, separator: ',' }))
  audio_full_ids: String[]) {
    return this.audioService.getAudioAdds(audio_full_ids)
  }

}
