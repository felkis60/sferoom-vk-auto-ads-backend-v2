import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AudioController } from './audio.controller';
import { AudioService } from './audio.service';
import { VkModule } from 'src/vk/vk.module';

@Module({
  imports: [VkModule, HttpModule],
  controllers: [AudioController],
  providers: [AudioService],
})
export class AudioModule {}
