import { Inject, Injectable } from '@nestjs/common';
import { VK_API } from 'src/constants';
import Bottleneck from 'bottleneck';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';


@Injectable()
export class AudioService {
  constructor(
    @Inject(VK_API) private vk: any,
    private httpService: HttpService) {
      this.vkAddsLimiter = new Bottleneck({
        maxConcurrent: 10,
        minTime: 300,
      });
  }

  private readonly vkAddsLimiter: any;

  parsePlaylistLink(playlist_link) {
    let p, playlist
    p = playlist_link.match(/^https:\/\/m.vk.com\/audio\?act=audio_playlist(-?\d+)_(\d+)/)
    playlist = { owner_id: p[1], playlist_id: p[2] }
    p = playlist_link.match(/&access_hash=(.+)&api_view=.+$/)
    playlist.access_key = p ? p[1] : ''
    return playlist
  }

  async getAudiosPlaylist(playlist) {
    const res = await this.vk.audio_api.request('audio.get', {
      owner_id: playlist.owner_id,
      album_id: playlist.playlist_id,
      access_key: playlist.access_key,
      count: 200,
    })
    return res.response.items
  }

  async getAudioCount(audio_full_id: any) {
    let matches, count = 0
    let { data } = await lastValueFrom(this.httpService.get(`https://m.vk.com/like?act=members&object=audio${audio_full_id}`))
    matches = [...data.matchAll(/<a class="pg_link" href="\/like\?act=members&object=audio-?\d+_\d+&offset=(\d+)">/g)]
    if (matches.length) {
      count = parseInt(matches[matches.length - 1][1], 10)
      data = (await lastValueFrom(this.httpService.get(`https://m.vk.com/like?act=members&object=audio${audio_full_id}&offset=${count}`))).data
    }
    count += (data.match(/<a class="inline_item" href="/g) || []).length
    return count
  }

  async getAudioAdds(audio_full_ids) {
    
    let res
    let counts = {}
  
    await Promise.all(audio_full_ids.map(async (id) => {
      try {
        res = await this.vkAddsLimiter.schedule(async () => { return await this.getAudioCount(id) });
        counts[id] = res
      } catch (e) {
        console.log('vk adds error: ' + e)
      }
    }))
  
    let count = Object.values(counts).reduce((a: any, b: any) => a + b, 0)
    return { count: count, counts: counts }
  }
  

}
