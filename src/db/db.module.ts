import { Module } from '@nestjs/common';
import { Pool } from 'pg';
import { PG_CONNECTION } from '../constants';
import { ConfigService } from '@nestjs/config';

const dbProvider = {
  provide: PG_CONNECTION,
  // useFactory: (config: ConfigService) => { 
  //     return new Pool({
  //   user: 'postgres',
  //   host: '91.240.85.80',
  //   database: 'SferoomVKAds',
  //   password: config.get('POSTGRES_PASSWORD'),
  //   port: 5432,
  // })},
  useFactory: (config: ConfigService) => {
      return new Pool({
    user: 'felkis60',
    host: '46.229.220.157',
    database: 'smmmusic',
    password: '60faabee',
    port: 5432,
  })},
  inject: [ConfigService],
};

@Module({
    providers: [dbProvider],
    exports: [dbProvider]
})
export class DbModule {}
