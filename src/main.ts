import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: "*"
  });
  app.setGlobalPrefix('api/v2/vk-ads');
  await app.listen(8301);
}
bootstrap();
