import { Controller, Get, Post, Body, Param, Query } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('access_token')
  set(@Body() params: any) {
    return this.userService.setUserToken(params.id, params.accessToken);
  }

  @Get('groups')
  get(@Query() query: any) {
    return this.userService.getUserGroups(query)
  }

}
