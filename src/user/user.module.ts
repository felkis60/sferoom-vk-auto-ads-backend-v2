import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { DbModule } from '../db/db.module';
import { VkModule } from 'src/vk/vk.module';

@Module({
  imports: [DbModule, VkModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
