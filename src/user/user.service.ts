import { Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION, VK_API } from 'src/constants';


@Injectable()
export class UserService {
  constructor(@Inject(PG_CONNECTION) private conn: any, @Inject(VK_API) private vk: any) {}

  async setUserToken(id, access_token) {
    await this.conn.query('update public.users set access_token_vk_ads=$1 where id=$2', [access_token, id]);
  }

  async getUserGroups(query) {
    return await this.vk.user_api.request('groups.get', query)
  }

  async getAudiosPlaylist(playlist) {
    const res = await this.vk.audio_api.request('audio.get', {
      owner_id: playlist.owner_id,
      album_id: playlist.playlist_id,
      access_key: playlist.access_key,
      count: 200,
    })
    return res.response.items
  }

}
