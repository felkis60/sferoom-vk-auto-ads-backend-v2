const axios = require('axios');
const Bottleneck = require('bottleneck');
const fs = require('fs');
const vk_userAgent = 'KateMobileAndroid/56 lite-460 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en)';

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

class TokenManager {
    constructor(tokens, minTime) {
        this.tokens = tokens
        this.minTime = minTime + 20 // +20 - запас
        this.index = 0
        this.updatedAt = new Date().getTime();
    }
    async getToken() {
        if (this.index == 0) {
            let dt = this.minTime - (new Date().getTime() - this.updatedAt)
            if (dt > 0) { await sleep(dt) }
            this.updatedAt = new Date().getTime()
        }
        this.index = (this.index + 1) % this.tokens.length
        return this.tokens[this.index]
    }
}

class VKApiAgent {
    constructor(input, bottleneck_params = { maxConcurrent: 1, minTime: 300 }, v = 5.175) {
        if (typeof (input) === 'string') {
            const tokens = JSON.parse(fs.readFileSync(input, 'utf8')).map(r => r.kate.token);
            this.tokenManager = new TokenManager(tokens, 3000)
        } else if (input instanceof Array) {
            this.tokenManager = new TokenManager(input, 3000)
        } else {
            this.tokenManager = null
        }
        this.limiter = new Bottleneck(bottleneck_params);
        this.v = v
    }
    async request(method, params) {
        params.v = this.v
        if (this.tokenManager) {
            for (let i = 0; i < this.tokenManager.tokens.length + 5; i++) { // +5 - запас
                const token = await this.tokenManager.getToken()
                params.access_token = token
                let result = await this.limiter.schedule(async () => {
                    let { data } = await axios(`https://api.vk.com/method/${method}?` + new URLSearchParams(params), {
                        method: 'GET',
                        headers: { 'User-Agent': vk_userAgent }
                    });
                    return data
                });
                if ('response' in result) {
                    return result
                } else {
                    console.log(result.error);
                }
            }
        } else {
            for (let i = 0; i < 5; i++) { // +5 - запас
                let result = await this.limiter.schedule(async () => {
                    let { data } = await axios(`https://api.vk.com/method/${method}?` + new URLSearchParams(params), {
                        method: 'GET',
                        headers: { 'User-Agent': vk_userAgent }
                    });
                    return data
                });
                if ('response' in result) {
                    return result
                } else {
                    console.log(result.error);
                }
            }
        }
        return {}
    }
}

module.exports = {
    VKApiAgent
};