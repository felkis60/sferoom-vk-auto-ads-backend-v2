import { Module } from '@nestjs/common';
import { VKApiAgent } from 'src/vk/VKApiAgentAxios.js';
import { VK_API } from '../constants';
import { ConfigService } from '@nestjs/config';

const vkProvider = {
  provide: VK_API,
  useFactory: (config: ConfigService) => {
    return { 
      audio_api: new VKApiAgent('./vkAudioApiKeys.json'),
      manager_api: new VKApiAgent([config.get('MANAGER_TOKEN')]),
      user_api: new VKApiAgent(null),
    }
  },
  inject: [ConfigService],
};

@Module({
    providers: [vkProvider],
    exports: [vkProvider]
})
export class VkModule {}
